variable "domain_name" {
  type        = string
  description = "domain name"
  default     = "aaronlong.tech"
}

variable "region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}
