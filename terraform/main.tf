terraform {
  backend "s3" {
    bucket = "iac-backend"
    key    = "aaronlong.tech"
    region = "us-east-1"
  }
}

locals {
  resource = tolist([
    "${aws_s3_bucket.b.arn}",
    "${aws_s3_bucket.b.arn}/*"
  ])
  s3_origin_id       = "${var.domain_name}-origin"
  root_user_arn      = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
  domain_aws_valid   = replace(var.domain_name, ".", "_")
  edge_File_location = pathexpand("${path.root}/../lambda-edge")
}

data "aws_caller_identity" "current" {}

data "aws_route53_zone" "domain" {
  name         = var.domain_name
  private_zone = false
}

provider "aws" {
  region  = var.region
  version = "~> 3.17"
}

resource "aws_iam_user" "ci_cd_user" {
  name = "personal-site"
  path = "/ci-cd/"

}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "${var.domain_name}-oai"
}

resource "aws_s3_bucket" "b" {
  bucket = var.domain_name
  acl    = "private"
}

resource "aws_s3_bucket_policy" "b" {
  bucket = aws_s3_bucket.b.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "ListBucket",
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : [
            aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
            data.aws_caller_identity.current.arn,
            aws_iam_user.ci_cd_user.arn,
            local.root_user_arn

          ]
        },
        "Action" : "s3:ListBucket",
        "Resource" : local.resource,
      },
      {
        "Sid" : "ReadObjects",
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : [
            aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
            data.aws_caller_identity.current.arn,
            aws_iam_user.ci_cd_user.arn,
            local.root_user_arn
          ]
        },
        "Action" : [
          "s3:GetObject",
          "s3:GetObjectVersion"
        ],
        "Resource" : local.resource,
      },
      {
        "Sid" : "ModifyObject",
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : [
            data.aws_caller_identity.current.arn,
            aws_iam_user.ci_cd_user.arn,
            local.root_user_arn
          ]
        },
        "Action" : [
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        "Resource" : local.resource,
      },
      {
        "Sid" : "GeneralDenyAll",
        "Effect" : "Deny",
        "Principal" : "*",
        "Action" : "s3:*",
        "Resource" : local.resource,
        "Condition" : {
          "StringNotLike" : {
            "aws:PrincipalArn" : [
              aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn,
              data.aws_caller_identity.current.arn,
              aws_iam_user.ci_cd_user.arn,
              local.root_user_arn
            ]
          }
        }
      }
    ]
  })
}

resource "aws_acm_certificate" "cert" {
  domain_name = var.domain_name

  subject_alternative_names = ["*.${var.domain_name}"]

  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "cert_record" {

  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.domain.zone_id
}

resource "aws_route53_record" "cloudfront_ip4" {
  type    = "A"
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = var.domain_name

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "cloudfront_ip6" {
  type    = "AAAA"
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = var.domain_name

  alias {
    name                   = aws_cloudfront_distribution.distribution.domain_name
    zone_id                = aws_cloudfront_distribution.distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_iam_role" "lambda_edge_exec" {
  name = "${var.domain_name}-edge-lambda-iam"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : "sts:AssumeRole",
        "Principal" : {
          "Service" : [
            "lambda.amazonaws.com",
            "edgelambda.amazonaws.com"
          ]
        },
        "Effect" : "Allow"
      }
    ]
  })
}

data "external" "build_edge_function" {
  working_dir = local.edge_File_location
  program     = ["sh", abspath("${path.root}/../lambda-edge/deploy.sh")]
}

// https://advancedweb.hu/how-to-use-lambda-edge-with-terraform/
resource "aws_lambda_function" "lambda_edge" {
  depends_on = [data.external.build_edge_function]
  # ...
  filename      = abspath("${path.root}/../lambda-edge/dist/app.zip")
  function_name = "${local.domain_aws_valid}-edge-uri-resolver"
  role          = aws_iam_role.lambda_edge_exec.arn
  handler       = "app.handler"
  runtime       = "nodejs12.x"

  publish = true
}

resource "aws_iam_user_policy" "lambda_edge_ci_cd_policy" {
  name = "${local.domain_aws_valid}-edge-ci-cd-policy"
  user = aws_iam_user.ci_cd_user.name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Action" : [
          "lambda:GetFunction",
          "lambda:GetFunctionConfiguration",
          "lambda:ListVersionsByFunction",
          "lambda:PublishVersion",
          "lambda:UpdateFunctionCode",
          "lambda:UpdateFunctionConfiguration",
          "lambda:PublishLayerVersion"
        ],
        "Effect" : "Allow",
        "Resource" : aws_lambda_function.lambda_edge.arn
      }
    ]
  })
}

resource "aws_cloudfront_distribution" "distribution" {
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Some comment"
  default_root_object = "index.html"

  aliases = [var.domain_name]
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  origin {
    origin_id   = var.domain_name
    domain_name = aws_s3_bucket.b.bucket_regional_domain_name
    s3_origin_config {

      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.domain_name

    forwarded_values {
      query_string = true

      cookies {
        forward = "none"
      }
    }

    min_ttl     = 0
    default_ttl = 3600
    max_ttl     = 86400

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = aws_lambda_function.lambda_edge.qualified_arn
    }
  }

  price_class = "PriceClass_100"

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method  = "sni-only"
  }
}

