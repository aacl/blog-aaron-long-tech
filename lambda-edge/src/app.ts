import { CloudFrontRequestEvent, CloudFrontRequest } from "aws-lambda";

/**
 * S3 doesn't support index.html to base url redirects
 * @param input 
 */
function addIndexToRoot(input: string): string {
    return input.replace(/\/$/, '/index.html');
}

/**
 * Update requet's url
 * @param uri - new uri
 * @param request - request object
 */
function updateRequestUri(
    uri: string,
    request: CloudFrontRequest
): CloudFrontRequest {
    return {
        ...request,
        uri
    }
}


// https://aws.amazon.com/blogs/compute/implementing-default-directory-indexes-in-amazon-s3-backed-amazon-cloudfront-origins-using-lambdaedge/
export async function handler(
    event: CloudFrontRequestEvent
): Promise<CloudFrontRequest> {
    const request = event.Records[0].cf.request;
    const newUrl = addIndexToRoot(request.uri);

    return updateRequestUri(newUrl, request);
}
