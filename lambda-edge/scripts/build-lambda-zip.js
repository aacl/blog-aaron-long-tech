const fs = require('fs');
const archiver = require('archiver');

async function main() {
    const cwd = process.cwd();
    const output = fs.createWriteStream(cwd + '/dist/app.zip');
    const archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });

    archive.pipe(output);
    archive.directory('dist/js', false);
    await archive.finalize();
    output.close();
}

main();
